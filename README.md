tuner is a set of utilities to train zurichess evaluation function

Prerequisites
===

Feature extractor, extract.go, needs latest version of the Go compiler
installed which can be downloaded from the official [site](https://golang.org/dl).

Weights trainer, tf.py, needs [Python 2.7](https://www.python.org/),
[NumPy](http://www.numpy.org/), and [TensorFlow](https://www.tensorflow.org/).


Tuning
===

First download and extract tuning data from
https://bitbucket.org/zurichess/tuner/downloads/tuner.7z.
We will use quiet-labelled.epd.

Tuning is very simple once the prerequisites are installed

```bash
% go build -tags coach extract.go
% ./extract --input extract.go > testdata

% python tf.py --summaries_dir ./logdir --input testdata
% tensorboard --logdir ./logdir
```

tf.py will print an array with all weights that looks similar to the array below.
Replace the new weights in [material.go](https://bitbucket.org/zurichess/zurichess/src/master/engine/material.go).


```
{M: 349, E: 9222}, {M: 10275, E: 8055}, {M: 45008, E: 32250}, {M: 46216, E: 33818}, {M: 61509, E: 63418}, {M: 147223, E: 103476}, {M: 3764, E: 6398}, {M: 2743, E: 3876}, 
{M: 815, E: 1832}, {M: 1196, E: 407}, {M: 1053, E: 407}, {M: 823, E: 339}, {M: 206, E: 1084}, {M: -100, E: -829}, {M: -130, E: 294}, {M: -146, E: -143},
{M: -171, E: -72}, {M: 56, E: -88}, {M: 79, E: 88}, {M: 303, E: 48}, {M: -363, E: -48}, {M: 43, E: 161}, {M: -1910, E: 927}, {M: -1567, E: 281},
{M: -788, E: 657}, {M: -642, E: 35}, {M: -1040, E: -73}, {M: 3115, E: -1172}, {M: 1783, E: -1635}, {M: -1459, E: -1987}, {M: -1599, E: -18}, {M: -2044, E: 222},
[...]
```
