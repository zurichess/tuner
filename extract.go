// Copyright 2014-2016 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// extract.go is a tool to extract features from a set of positions.
//
// Usage:
//    % go build -tags coach extract.go
//    % ./extract --input input.epd > testdata
//
// Must be compiled with -tags coach in order to activate special
// code in the engine that keeps track of features.
// Input is a list of epds with c9 containg the game result.
// Output a CSV file with row format: result, phase, feature1, ..., featureN.
package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"sync"

	"bitbucket.org/zurichess/notation"
	"bitbucket.org/zurichess/notation/epd"
	. "bitbucket.org/zurichess/zurichess/engine"
)

var (
	inputPath = flag.String("input", "", "input file")

	c9ToScore = map[string]float64{
		"1":       1,
		"1-0":     1,
		"0.5":     0.5,
		"1/2-1/2": 0.5,
		"0":       0,
		"0-1":     0,
	}

	hit, miss int
)

type datum struct {
	pos    *Position
	result float64
}

func readData(input string) ([]datum, error) {
	f, err := os.Open(input)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	var data []datum
	scanner := notation.NewEPDScanner(f, &epd.Parser{})
	for scanner.Scan() {
		if epd := scanner.EPD(); epd != nil {
			d := datum{
				pos:    epd.Position,
				result: c9ToScore[epd.Comment["c9"]],
			}
			// Evaluate just the top of the tree.
			data = append(data, d)
		}
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return data, err
}

func main() {
	flag.Parse()
	log.SetFlags(log.Lshortfile | log.Ltime)

	// Read input
	if *inputPath == "" {
		log.Fatalf("Missing --input flag")
	}
	data, err := readData(*inputPath)
	if err != nil {
		log.Fatalf("Cannot read %s: %v", *inputPath, err)
	}
	log.Printf("Read %d positions from %s", len(data), *inputPath)

	// Setup the
	GlobalHashTable = NewHashTable(2)
	wg := &sync.WaitGroup{}
	ch := make(chan struct{}, 32)
	pr := make(chan struct{}, 1)

	// Play all positions.
	wg.Add(len(data))
	for i := range data {
		if i%10000 == 0 {
			log.Printf("starting %d", i)
		}

		if data[i].pos.MinorsAndMajors(White) == 0 && data[i].pos.MinorsAndMajors(Black) == 0 {
			wg.Done()
			continue
		}

		ch <- struct{}{}
		go func(i int) {
			// Play the position.
			d := &data[i]
			eval := EvaluatePosition(d.pos)
			phase := float64(Phase(d.pos)) / 256

			buf := &bytes.Buffer{}
			fmt.Fprint(buf, data[i].result, " ", phase)
			for _, v := range eval.Accum.Values {
				fmt.Fprint(buf, " ", v)
			}
			fmt.Fprintln(buf)

			<-ch

			// Print eval. Synchronize printing.
			pr <- struct{}{}
			os.Stdout.Write(buf.Bytes())
			<-pr

			d.pos = nil
			wg.Done()
		}(i)
	}
	wg.Wait()
}
