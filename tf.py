"""Tensorflow program to train zurichess evaluation function.

Usage:
    % python tf.py -h
    % python tf.py --summaries_dir ./tmpdir --input testdata
    % tensorboard --logsdir ./tmpdir

Input is CSV file each line contains:
    result, phase, feature1, ..., featureN

"""

import argparse
import numpy as np
import tensorflow as tf


FLAGS = None


def train():
    """Sets up the network."""

    data = np.loadtxt(FLAGS.input, dtype='float32')
    y_data = data[:, 0]
    p_data = data[:, 1]
    x_data = data[:, 2:]

    num_features = len(x_data[0])
    print 'Read %d (%d) records of %d inputs' % (len(x_data), len(y_data), num_features)

    session = tf.InteractiveSession()

    midgame = tf.Variable(tf.random_uniform([num_features]), name='midgame')
    endgame = tf.Variable(tf.random_uniform([num_features]), name='endgame')
    # Figure values are based on current positions in the feature vector.
    tf.scalar_summary(['figure_1pawn', 'figure_2knight', 'figure_3bishop', 'figure_4rook', 'figure_5queen'],
                      (midgame[1:6] + endgame[1:6]) / 2)
    tf.scalar_summary(['mobility_1pawn', 'mobility_2knight', 'mobility_3bishop', 'mobility_4rook', 'mobility_5queen'],
                      (midgame[8:13] + endgame[8:13]) / 2)

    # Build the network.
    phase = tf.constant(p_data, name='phase')
    ym = tf.reduce_sum(x_data * midgame, 1) * (1 - phase)
    ye = tf.reduce_sum(x_data * endgame, 1) * phase
    y = tf.sigmoid((ym + ye) / 2)

    sum = tf.abs(midgame) + tf.abs(endgame)
    regularization = 1e-3 * (
        tf.reduce_mean(sum[14:78]) +  # pawns psqt
        tf.reduce_mean(sum[94:142]))  # knight, bishop, king psqt
    error = tf.square(y - y_data)
    loss = regularization + tf.reduce_mean(error)
    tf.scalar_summary('loss', loss)

    optimizer = tf.train.AdamOptimizer(learning_rate=0.1)
    train = optimizer.minimize(loss)

    merged = tf.merge_all_summaries()
    train_writer = tf.train.SummaryWriter(
        FLAGS.summaries_dir + '/train', session.graph)
    tf.initialize_all_variables().run()

    # Fit the plane.
    best, last = 1, 0
    for step in xrange(0, FLAGS.steps):
        summary, _ = session.run([merged, train])
        train_writer.add_summary(summary, step)

        if step % 10 == 9:
            train_writer.flush()
            diff = session.run(loss)
            print step, diff, session.run(y)[0:5] - y_data[0:5]

            if step - last >= 50 and diff < best:
                best, last, l = diff, step, []
                m, e = session.run([midgame, endgame])
                for i in range(len(m)):
                    l.append('{M: %d, E: %d}' %
                             (int(m[i] * 10000), int(e[i] * 10000)))

                for s in xrange(0, len(l), 8):
                    e = min(s + 8, len(l))
                    print ', '.join(l[s:e]), ','

    train_writer.close()
    session.close()


def main(_):
    if tf.gfile.Exists(FLAGS.summaries_dir):
        tf.gfile.DeleteRecursively(FLAGS.summaries_dir)
        print 'Deleted', FLAGS.summaries_dir
    tf.gfile.MakeDirs(FLAGS.summaries_dir)
    train()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', type=str, required=True,
                        help='Input csv file')
    parser.add_argument('--summaries_dir', type=str, required=True,
                        help='Summaries directory')
    parser.add_argument('--steps', type=int, default=1500,
                        help='Number of training steps to run')
    FLAGS = parser.parse_args()
    tf.app.run()
